export class CProducto {
    public codigo: number;
    public nombre: string;
    public descripcion: string;
    public valor: number;
    public stock: number;

    constructor() {
        this.codigo = 0;
        this.nombre = "";
        this.descripcion = "";
        this.valor = 0;
        this.stock = 0;
    }
}