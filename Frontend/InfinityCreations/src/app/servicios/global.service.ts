import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  public BASE_URL: string;
  public BASE_SERVER: string;
  public headerToken: HttpHeaders;
  public headers: HttpHeaders;

  constructor(public http: HttpClient) {
     this.BASE_URL = "http://localhost:54295/api/";
     this.BASE_SERVER = "http://localhost:54295/api/";
    
    this.headers = new HttpHeaders(
    { 
      'Content-Type': 'application/json', 
      'Access-Control-Allow-Origin': '*', 
      'Access-Control-Allow-Methods': 'POST: GET: OPTIONS: PUT', 
      'Authorization': 'Bearer ', 
      'mode': 'no-cors' 
    });
    
  }




  public get(url: string, id?: string) {
    if (id === undefined) {
      id = "";
    }
    let api = this.BASE_URL + url + id;
    return new Promise((resolve, reject) => {
      this.http.get(api, { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public getTwoId(url: string, id?: string, id2?: string) {
    if (id === undefined) {
      id = "";
    }
    if (id2 === undefined) {
      id2 = "";
    }
    let api = this.BASE_URL + url + id + "/" + id2;
    return new Promise((resolve, reject) => {
      this.http.get(api, { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public post(url: string, object: any) {
    let api = this.BASE_URL + url;
    return new Promise((resolve, reject) => {
      this.http.post(api, JSON.stringify(object), { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public postList(url: string, object: any[]) {
    let api = this.BASE_URL + url;
    return new Promise((resolve, reject) => {
      this.http.post(api, JSON.stringify(object), { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public postWithId(url: string, object: any, id?: string) {
    if (id === undefined) {
      id = "";
    }
    let api = this.BASE_URL + url + id;
    return new Promise((resolve, reject) => {
      this.http.post(api, JSON.stringify(object), { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public put(url: string, object: any, id?: string) {
    if (id === undefined) {
      id = "";
    }
    let api = this.BASE_URL + url + id;
    return new Promise((resolve, reject) => {
      this.http.put(api, JSON.stringify(object), { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      })
    });
  }

  public putTwoId(url: string, object: any, id?: string, id2?: string) {
    if (id === null) {
      id = "";
    }
    let api = this.BASE_URL + url + id + "/" + id2;
    return new Promise((resolve, reject) => {
      this.http.put(api, JSON.stringify(object), { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      })
    });
  }

  public putThreeId(url: string, object: any, id?: string, id2?: string, id3?: string) {
    if (id === null) {
      id = "";
    }
    let api = this.BASE_URL + url + id + "/" + id2 + "/" + id3;
    return new Promise((resolve, reject) => {
      this.http.put(api, JSON.stringify(object), { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      })
    });
  }

  public delete(url: string, id: string) {
    let api = this.BASE_URL + url + id;
    return new Promise((resolve, reject) => {
      this.http.delete(api, { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public deleteTwoId(url: string, id: string, id2: string) {
    let api = this.BASE_URL + url + id + "/" + id2;
    return new Promise((resolve, reject) => {
      this.http.delete(api, { headers: this.headers }).subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public postDocuments(url: string, object: FormData) {
    let api = this.BASE_URL + url;
    let headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return new Promise((resolve, reject) => {
      this.http.post(api, object, { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
