import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { GlobalService } from 'src/app/servicios/global.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from './modal/modal.component';
import { Subscription } from 'rxjs';
import { CProducto } from 'src/app/clases/CProducto';



@Component({
  selector: 'app-crear-factura',
  templateUrl: './crear-factura.component.html',
  styleUrls: ['./crear-factura.component.scss']
})
export class CrearFacturaComponent implements OnInit {

  public productostotal:number;
  public total:number;
  lstProductos : CProducto[];
  productos: CProducto[];
  public subscriptions: Subscription[];
  myControl = new FormControl();
  constructor(private globalService: GlobalService,private modalService: BsModalService,) {
    this.subscriptions = [];
    this.lstProductos = [];
    this.productos = [];
   }

  ngOnInit() {
  }

  listarProductos(indices: string[]){
    this.total = 0;
    indices.forEach(x =>{
      this.productos.push(this.lstProductos[+x]);
    })

    this.productos.forEach(x=>{
      this.total += x.valor;
    })
  }

  public openModal() {
    this.globalService.get("producto").then((res: any) => {
    this.lstProductos = [];
    this.lstProductos = res.message;
      const initialState = {
        lstProductos: this.lstProductos
      }
      this.modalService.show(ModalComponent, { initialState, keyboard: false, ignoreBackdropClick: true, class: "modal-xl" });
      this.subscriptions.push(
        this.modalService.onHidden.subscribe((reason: any) => {
          if (reason) {
            
            this.listarProductos(reason.split("-"));
            this.unsubscribe();
          }
        })
      );
    }, (err) => {
      //this.toastr.error(err.message, "", { timeOut: err.message.length * 100 });
    });
  }

  public unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

}
