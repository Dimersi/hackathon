import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CProducto } from 'src/app/clases/CProducto';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit,  AfterViewInit {

  public lstProductos: CProducto[];
  public seleccion: number[];
  

  constructor(private modalService: BsModalService, public bsModalRef: BsModalRef) { 
    this.seleccion = [];
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    
  }

  Seleccionar(index: number){
    var i = this.seleccion.findIndex(x => x==index) ;
    if(i >= 0){
      this.seleccion.splice(i,1)
    }else{
      this.seleccion.push(index);
    }
  }
  Aceptar() {
    this.modalService.setDismissReason(this.seleccion.join("-"));
    this.bsModalRef.hide();
  }

  Cancelar() {
    this.modalService.setDismissReason("true");
    this.bsModalRef.hide();
  }


}
