﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;


namespace Logica
{
   public static class LProductos
    {
        public static List<Entidades.Producto> ListarProductos()
        {
            List<Entidades.Producto> lstResult = new List<Entidades.Producto>();

            using (InfinityCreationsEntities _context = new InfinityCreationsEntities())
            {
                var lstArticulos = _context.Productoes.ToList();
                lstArticulos.ForEach(x => lstResult.Add(ToLocalEntity(x)));
            }

            return lstResult;
        }

        public static Entidades.Producto ConsultarPorId(int id)
        {
            Entidades.Producto result = new Entidades.Producto();

            using (InfinityCreationsEntities _context = new InfinityCreationsEntities())
            {
                var producto = _context.Productoes.FirstOrDefault(x => x.codigo == id);
                result = ToLocalEntity(producto);
            }

            return result;
        }

       

        public static Entidades.Producto ToLocalEntity(Datos.Producto producto)
        {
            return producto == null ? null : new Entidades.Producto
            {
                codigo = producto.codigo,
                nombre = producto.nombre,
                descripcion = producto.descripcion,
                valor = producto.valor,
                stock = producto.stock
            };
        }

        public static Datos.detalleFactura ToTable(Entidades.DetalleFactura datos)
        {
            return datos == null ? null : new Datos.detalleFactura
            {
                codigoProducto = datos.codigoProducto,
                codigoFactura = datos.codigoFactura,
                cantidadFacturar = datos.cantidadFacturar
            };
        }

        public static void GuardarFactura(Entidades.DetalleFactura datos)
        {
            using (InfinityCreationsEntities _context = new InfinityCreationsEntities())
            {
                _context.detalleFacturas.Add(ToTable(datos));
                 _context.SaveChanges();
            }
        }

        public static void EditarCantidadId(int IdProductos, int cantidadFacturada)
        {
            Entidades.Producto result = new Entidades.Producto();

            using (InfinityCreationsEntities _context = new InfinityCreationsEntities())
            {
                var producto = _context.Productoes.FirstOrDefault(x => x.codigo == IdProductos);
                producto.stock = producto.stock - cantidadFacturada;
                if(producto.stock < 0){
                    //mandar alerta se pedido de factura
                    producto.stock = producto.stock*(-1);
                }
                _context.SaveChanges();
            }
        }

       
    }
}
