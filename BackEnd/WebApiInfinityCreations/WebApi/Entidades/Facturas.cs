﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Facturas
    {
        public int codigo { get; set; }
        public string codigoCliente { get; set; }
        public decimal total { get; set; }
        public DateTime fecha { get; set; }
    }
}
