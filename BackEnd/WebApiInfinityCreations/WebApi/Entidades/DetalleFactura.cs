﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class DetalleFactura
    {
        public int codigoProducto { get; set; }
        public int codigoFactura { get; set; }
        public int cantidadFacturar { get; set; }
    }
}
