﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Entidades;
using Logica;

namespace WebApi.Controllers
{
    
    [Route("api/Producto/")]
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class ProductoController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var lstProductos = LProductos.ListarProductos();
                return Json(new { success = true, message = lstProductos });
            }
            catch (Exception exc)
            {
                return Json(new { success = false, message = "Error!: " + exc.Message });
            }
        }

        // GET api/productos/{id}
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var producto = LProductos.ConsultarPorId(id);
                return Json(new { success = true, message = producto });
            }
            catch (Exception exc)
            {
                return Json(new { success = false, message = "Error!: " + exc.Message });
            }
        }

    }
}
